package main

// import (
// 	"context"
// 	"encoding/json"
// 	"net/http"
// 	"time"

// 	"github.com/mongodb/mongo-go-driver/bson"
// 	"gitlab.com/akita/akita"
// 	"gitlab.com/akita/util/tracing"
// )

// type IPS struct {
// 	Start, End    akita.VTimeInSec
// 	BinDuration   akita.VTimeInSec
// 	StartCount    []akita.VTimeInSec
// 	CompleteCount []akita.VTimeInSec
// }

// func handleIPS(w http.ResponseWriter, r *http.Request) {
// 	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Second)
// 	defer cancel()

// 	StartTime, EndTime := GetSimulationStartEndTime()

// 	binCount := 100
// 	ips := IPS{
// 		Start:         StartTime,
// 		End:           EndTime,
// 		BinDuration:   (EndTime - StartTime) / akita.VTimeInSec(binCount),
// 		StartCount:    make([]akita.VTimeInSec, binCount+1),
// 		CompleteCount: make([]akita.VTimeInSec, binCount+1),
// 	}

// 	cursor, err := collection.Find(ctx,
// 		bson.D{bson.E{Key: "kind", Value: "kind"}})
// 	dieOnErr(err)
// 	for cursor.Next(ctx) {
// 		var task tracing.Task
// 		err := cursor.Decode(&task)
// 		dieOnErr(err)

// 		startBin := int((task.StartTime - StartTime) / ips.BinDuration)
// 		ips.StartCount[startBin]++

// 		endBin := int((task.EndTime - StartTime) / ips.BinDuration)
// 		ips.CompleteCount[endBin]++
// 	}

// 	for i := 0; i < binCount+1; i++ {
// 		ips.StartCount[i] /= ips.BinDuration
// 		ips.CompleteCount[i] /= ips.BinDuration
// 	}

// 	var rsp []byte
// 	rsp, err = json.Marshal(ips)
// 	dieOnErr(err)
// 	_, err = w.Write(rsp)
// 	dieOnErr(err)
// }
